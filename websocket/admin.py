from websocket.models import ChatMessage, Connection
from django.contrib import admin

# Register your models here.

admin.site.register(ChatMessage)
admin.site.register(Connection)
